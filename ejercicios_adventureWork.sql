--------------------------------------
--- Ejercicios AdventureWorks --------
--------------------------------------
-- Alumna: 
-- Mayerling Nazareth Aguilera Suarez
-- Cedula Venezolana: 24290965
-- Correo: mayerlingnas@gmail.com
--------------------------------------


----------- Parte 1 -------------
---------------------------------

-- 1. Escribe una consulta select que liste los customers con su ID. Incluye tambi�n los appelidos, nombres y su Company Name. 
SELECT CONCAT (p.FirstName, '', P.MiddleName) AS Names
		,p.LastName AS 'LastName'
		,s.Name AS 'Company Name'
FROM sales.Customer c
JOIN Person.Person p ON c.PersonID = p.BusinessEntityID 
JOIN Sales.Store s ON c.StoreID = s.BusinessEntityID


-- 2.Escribe una consulta select para obtener los name, producto number y color para cada producto. 
SELECT p.Name,
		p.ProductNumber,
		p.color
FROM Production.Product p 

-- 3.Escribe una consulta select para obtener el Customer ID number y sales order ID number de la tabla SalesLT.SalesOrderHeader. 
SELECT CustomerID,
	SalesOrderID
FROM  Sales.SalesOrderHeader

-- 4. Pregunta:
-- Por que debemos epecificar los nombres de las columnas al hacer una consulta en lugar de utilizar el asterisco. De almenos dos razones
/* Respuesta:
 * 1. No traer data innesaria a la consulta
 * 2. Optimizar costo y tiempo al traer los registros necesario
 */

------------ Parte 2 -------------
----------------------------------

-- 5. Escribe un query utilizando un where donde tengamos a todos los empleados(employees) en la tabla HumanResources.Employee que tienen un JobTitle = �Research� y �Development Engineer�. Muestra los campos business entity ID number, the login ID, and the title para cada caso.
SELECT BusinessEntityID
		,LoginID
		,JobTitle AS title 
FROM HumanResources.Employee e
WHERE  e.JobTitle IN ('Research and Development Engineer')


-- 6. Escribe un query con una sentencia where para todos los middlename = �J�  en la tabla Person.person. incluye los campos: firstname, middlename, lastname y businessentityid.
SELECT BusinessEntityID
		,FirstName
		,MiddleName
		,LastName
FROM Person.Person
WHERE middlename = 'J' 

-- 7. Escribe una consulta donde se muestren todas las columnas de la tabla Production.ProductCostHistory para las filas que hayan sido modificadas en Junio 17 del 2003.
SELECT *
FROM Production.ProductCostHistory
WHERE Convert(date,ModifiedDate) = '2003-06-17'

-- 8. Reescribe el query de la  pregunta anterior y selecciona solamente los empleados que no tiene un jobtitle de Research and Development Engineer
SELECT BusinessEntityID
		,LoginID
		,JobTitle AS title 
FROM HumanResources.Employee e
WHERE  e.JobTitle NOT IN ('Research and Development Engineer')
ORDER BY e.JobTitle

-- 9. Escribe una consulta donde obtengamos todas las filas de la tabla Person.person que fueron modificados despu�s del 29de diciembre de 2000. 
--    Muestra el business entity ID number, the firstname , middlename y lastname que hayan sido modificadas despu�s de esa fecha.
SELECT BusinessEntityID
		,FirstName
		,MiddleName
		,LastName
FROM Person.person 
WHERE Convert(date,ModifiedDate) > '2000-12-29'

-- 10. Reescribe el query anterior para verificar las filas que NO fueron modificadas el 29de diciembre de 2000. 
SELECT BusinessEntityID
		,FirstName
		,MiddleName
		,LastName
FROM Person.person 
WHERE Convert(date,ModifiedDate) != '2000-12-29'

-- 11. Reescribe la consulta del ejercicio 5 para sacar las filas que fueron modificadas en el mes de diciembre del a�o 2000. 
SELECT BusinessEntityID
		,LoginID
		,JobTitle AS title
		,ModifiedDate
FROM HumanResources.Employee e
WHERE  e.JobTitle IN ('Research and Development Engineer') AND  DATEPART(YEAR, ModifiedDate) = 2000


-- 12. Reescribe la consulta del ejercicio 5 para sacar las filas que NO  fueron modificadas en el mes de diciembre del a�o 2000. 
SELECT BusinessEntityID
		,LoginID
		,JobTitle AS title
		,ModifiedDate
FROM HumanResources.Employee e
WHERE  e.JobTitle IN ('Research and Development Engineer') AND  DATEPART(YEAR, ModifiedDate) != 2000

-- 13. Pregunta. Explica por que una clausula WHERE tiene que ser usada cuado realizamos consultas en SQL. 
/* Respuesta:
 * Para delimitar la busqueda segun sea lo necesario dividiendo los datos en partes progresivas mas peque�as
 */


------------ Parte 3 --------------
-----------------------------------

-- 14. Escribe un uqery que nos muestre el productID y nombre para cada producto de la tabla Production.product que empiecen con �Chain� 
SELECT ProductID
		,Name
FROM Production.product
WHERE Name LIKE 'Chain%'

-- 15. Escribe una consulta como la de la pregunta 1 donde el nombre del producto contenga la palabra �helmet�
SELECT ProductID
		,Name
FROM Production.product
WHERE Name LIKE '%helmet%'

-- 16. Utiliza el query anterior para obtener los productos que NO contienen la palabra �helmet� en su nombre. 
SELECT ProductID
		,Name
FROM Production.product
WHERE Name NOT LIKE '%helmet%'

-- 17. Escribe una consulta donde tengamos las columnas BusinessEntityID, FirstName, MiddleName, LastName de la tabla person.person
--     para que solo las filas que tienen como middlename E o B se muestren.
SELECT BusinessEntityID
		,FirstName
		,MiddleName
		,LastName
FROM Person.person 
WHERE MiddleName IN ('E','B')

/* 18. Explica la diferencia entre las dos consultas a continuaci�n: 
SELECT FirstName 
FROM Person.Person 
WHERE LastName LIKE 'Ja%es'; 
 
SELECT FirstName 
FROM Person.Person 
WHERE LastName LIKE 'Ja_es'; 

RESPUESTA: 
La primera consulta tiene el %, lo que quiere decir es que traera los nombres de las personas con apellidos que comiencen en Ja y terminen en es, sin importar
la cantidad de caracteres que posea en el medio la cadena, mientras que la segunda consulta traera los nombres de las personas cuyos apellido comiencie con
Ja, terminen con es y solo posea un (uno) carater en el medio que puede ser cualquiera
*/


----------- Parte 4 -------------
-----------------------------------

-- 19. Escribe una consulta con las columnas :
--     salesorderid, orderdate y totaldue donde las fechas de orden est�n entre 1 septeimbre 2001 y 30 septiembre 2001 y el totaldue sea mayor a 1000. 
SELECT SalesOrderID
		,OrderDate
		,TotalDue
FROM Sales.SalesOrderHeader
WHERE TotalDue > 1000 AND (Convert(date, OrderDate) BETWEEN '2001-09-01' AND '2001-09-30')

-- 20. Modifica el query de la pregunta 1 para obtener solo las ventas del 1 al 3 de septiembre y su totaldue sea mayor a 1000.  Muestra 3  posibles consultas que puedes realizar.
SELECT SalesOrderID
		,OrderDate
		,TotalDue
FROM Sales.SalesOrderHeader
WHERE TotalDue > 1000 AND (Convert(date, OrderDate) BETWEEN '2001-09-01' AND '2001-09-03')


-- 21. Escribe una consulta donde se muestren los campos SalesOrderID, OrderDate, TotalDue, de la tabla  Sales.SalesOrderHeader  
--     donde se tenga un totaldue >1000 y (salespersonID sea 279 o territoryid sea 6 ) 
SELECT SalesOrderID
		,OrderDate
		,TotalDue
		,salespersonID
		,territoryid
FROM Sales.SalesOrderHeader
WHERE TotalDue > 1000 AND (salespersonID = 279 OR TerritoryID = 6)

-- 22. Reescribe el query de la pregunta 3 incluyendo adem�s el territory id 4, es decir, que tenga en territoryid 4 y 6. 
SELECT SalesOrderID
		,OrderDate
		,TotalDue
		,salespersonID
		,territoryid
FROM Sales.SalesOrderHeader
WHERE TotalDue > 1000 AND salespersonID = 279 OR TerritoryID IN (4,6)

-- 23. Pregunta. Explica cuando debemos hacer uso del operador IN en una consulta.
/* Respuesta:
 * Cuando se desee evaluar si una expresion esta dentro de una lista de valores
 */

------------ Parte 5 -------------
----------------------------------

-- 24. Escribe un query donde despliegues las columnas the ProductID, Name, y Color 
--     donde no se tenga un color asignado para cada producto de la tabla production.product. 
SELECT ProductID
		,Name
		,Color
FROM production.product
WHERE Color IS NULL 

-- 25. Escribe un query donde se despliegues las columnas the ProductID, Name, y Color cuando el color  NO sea �blue�.
SELECT ProductID
		,Name
		,Color
FROM production.product
WHERE Color != 'Blue'

-- 26. Escribe un query mostrando las columnas despliegues las columnas the ProductID, Name,  style  size y color,
--	   donde los valores de style o size o color no tengan valores nulos. 
SELECT ProductID
		,Name
		,Style
		,Size
		,Color
FROM production.product
WHERE Style IS NOT NULL AND Size IS NOT NULL AND Color IS NOT NULL

------------ Parte 6 -------------
----------------------------------
-- 27. Escribe un query done se desplieguen las columnas BusinessEntityID, LastName, FirstName, MiddleName y ordenalos por  lastname, firstname y middlename.
SELECT BusinessEntityID
		,LastName
		,FirstName
		,MiddleName
FROM Person.person 
ORDER BY 2,3,4

-- 28. Modifica la consulta anterior para que el orden se realice de manera descendiente por cada unos de las columnas. 
SELECT BusinessEntityID
		,LastName
		,FirstName
		,MiddleName
FROM Person.person 
ORDER BY 2 DESC,3 DESC, 4 DESC

------------- Parte 7 --------------
-----------------------------------

-- 29. Escibe una consulta  utilizando la tabla person.address 
--     donde pongamos la direcci�n en una sola cadena de texto conteniendo las columnas AddressLine1, city y postalcode
SELECT DISTINCT CONCAT (AddressLine1, ' ', City, ' ',PostalCode) AS Address
FROM person.address


-- 30. Escribe una consulta de la tabla productino.product donde cambiemos el color de aquellos producto que tengan un color como valor nulo por el valor �NO COLOR�
UPDATE Production.product
SET Color = 'NO COLOR'
WHERE Color IS NULL 

SELECT ProductID
		,Color
		,Name
FROM Production.product 

-- 31. Modifica la consulta de la pregunta 2 para mostrar el ProductID y en una sola columna mostrar el nombre del producto y el color. 
SELECT ProductID
		,CONCAT(Color, ' - ', Name)
FROM Production.product 


-- 32. Escribe una consulta usando la tabla production.product poniendo en una sola columna el productid y el nombre del producto separados por un espacio
--     y nombrando a la columna como IDName. Para esto tendr�s que cambiar el formato de productid para que la consulta funcione correctamente. 
SELECT CONCAT(CONVERT(CHAR(4), ProductID), ' ', Name) AS IDName
FROM production.product
ORDER BY IDName

-------------- Parte 8 -------------
------------------------------------
-- 33. Escribe un query usando la tabla sales.specialoffer donde mostreoms la diferencia entre MinQty y MaxQty .
--     Muestra las columnas SpecialOfferID, Description y la diferencia solicitada.
SELECT SpecialOfferID
		,Description
		,(ISNULL(MaxQty, 0) - ISNULL(MinQty, 0)) AS Diferencia
FROM sales.specialoffer

-- 34.Escribe una consulta donde hagamos la multiplicaci�n entre la MinQty y DiscountPCT para sacar el precio de descuento por cantidad. 
--    Muestra las columnas SpecialOfferID, Description
SELECT SpecialOfferID
		,Description
		,(ISNULL(MinQty, 0) * DiscountPCT) AS 'Precio de descuento X cant'
FROM sales.specialoffer


-- 35. Realiza una consulta donde cambiemos el valor de MAxQty a 10 cuando los valores sean nulos y luego usar este valor para multiplicarlo 
--     con Discountpct y nombrar a la columna como Discount. Muestra las columnas SpecialOfferID, Description
SELECT SpecialOfferID
		,Description
		,(ISNULL(MaxQty, 10)  * DiscountPCT) AS Discount
FROM sales.specialoffer

-------------- Parte 9 -------------
------------------------------------
-- 36. Escribe un query que calcule el n�mero de d�as entre la fecha en la que se tom� una orden y la fecha en la que esta fue despachada al cliente.
--     Incluir los campos SalesOrderID, OrderDate, and ShipDate.
SELECT SalesOrderID
		,OrderDate
		,ShipDate
		,DATEDIFF(Day, OrderDate, ShipDate) AS 'Nro. Dias'
FROM Sales.salesorderheader


-- DEBO VER CUAL ES LA FECHA DE ENTREGA
-- 37. Escribe un query donde mosremos sOlo la fecha y no la hora de una orden y la fecha de entrega.  
SELECT SalesOrderID
		,Convert(date, OrderDate) AS'fechaOrden'
		,Convert(date, ShipDate) AS'fechaEntrega'
FROM Sales.salesorderheader


-- 38. Escribe una consulta que agregue 6 meses a las fechas de orden, muestra los campos salesorderid y orderdate. 
SELECT SalesOrderID
		,OrderDate
		,DATEADD(MONTH, 6, OrderDate)
FROM Sales.salesorderheader

-- 39. Escribe una consulta donde mostremos ela�o y mes de la fecha de orden. 
SELECT SalesOrderID
		,DATEPART(YEAR, OrderDate) AS 'A�o de Orden'
		,DATEPART(MONTH, OrderDate) AS 'Mes de Orden'
FROM Sales.salesorderheader


-- 40. Cambia el query de la pregunta anterior para mostrar el nombre del mes en lugar del numero. Para esto utilizamos la funci�n DateName(m,�columna de fecha�)
SELECT SalesOrderID
		,DATEPART(YEAR, OrderDate) AS 'A�o de Orden'
		,DATENAME(MONTH, OrderDate) AS 'Mes de Orden'
FROM Sales.salesorderheader

------------- Parte 10 -------------
------------------------------------
-- 41. Escribe una consulta donde redondeemos el valor de Subtotal para obtener un n�mero con solo 2 decimales. 
--     Para esto usamos la funci�n round(�nombrecolumna�,�numero decimales�)
SELECT SalesOrderID
		,round(SubTotal, 2) AS SubTotal
FROM Sales.salesorderheader

-- 42. Modifica el query del ejercicio 1 para obtener un valor entero, es decir un valor de subtota sin decimales. 
SELECT SalesOrderID
		,round(SubTotal, 0) AS SubTotal
FROM Sales.salesorderheader

-- 43. Calcula el valor de la ra�z cuadrada de SalesorderID. Para esto usamos la funci�n SQRT(�columna). 
SELECT SalesOrderID
		,SQRT(SubTotal)
FROM Sales.salesorderheader

------------- Parte 11 --------------
-------------------------------------
-- 44. Escribe un query usando la tabla sales.salesorderheader para sacar los valores de todas las filas que hayan tenido una orden en el a�o 2001.
SELECT *
FROM sales.salesorderheader
WHERE DATEPART(YEAR, OrderDate) = 2001  

-- 45. Escribe un query usando la tabla sales.salesorderheader donde mostrmos los campos salesorderid y orderdate y ordenemos la data por el mes y despu�s el a�o. 
SELECT SalesOrderID
		,OrderDate
FROM sales.salesorderheader
ORDER BY DATEPART(MONTH, OrderDate), DATEPART(YEAR, OrderDate)

-- 46. Escribe un query usando la tabla person.person donde mostremos las columnas persontype, firstame, middlename, lastname  y vamos a ordenar los datos
--     cuando tipo de persona este dentro de IN, SP,SC por el Lastname y si no est� dentro de estos ordenar por Firstname.  Hint: usar un case. 
SELECT PersonType
		,FirstName
		,MiddleName
		,LastName
FROM Person.person
ORDER BY 
CASE WHEN PersonType IN ('IN','SP', 'SC') THEN LastName END ASC,
CASE WHEN PersonType NOT IN ('IN','SP', 'SC') THEN FirstName END ASC

------------- Parte 12 --------------
-------------------------------------
-- 47. Realizar un query donde mostremos el jobtitle, birthdate, firstname, lastname haciendo la consulta a las tablas HumanResources.Employees y Person.Person.
SELECT e.JobTitle
		,e.BirthDate
		,p.FirstName
		,p.LastName
FROM Person.Person p
JOIN HumanResources.Employee e ON e.BusinessEntityID = p.BusinessEntityID


-- 48. Realizar una consulta donde mostremos las columnas  CustomerID, StoreID, TerritoryID, FirstName, MiddleName, LastName de las tablas Sales.Customer y Person.Person
SELECT c.CustomerID
		,c.StoreID
		,c.TerritoryID
		,p.FirstName
		,p.LastName
FROM Sales.Customer c
JOIN Person.Person p ON c.PersonID = p.BusinessEntityID 


-- 49. Utilicemos el query del ejercicio 2 para incluir el territorio de la tabla sales.salesorderheader
SELECT c.CustomerID
		,c.StoreID
		,c.TerritoryID
		,s.TerritoryID AS territorio
		,p.FirstName
		,p.LastName
FROM Sales.Customer c
JOIN Person.Person p ON c.PersonID = p.BusinessEntityID 
JOIN sales.salesorderheader s ON S.CustomerID = c.CustomerID


-- 50. Escribe una consulta que utilice campos de las tablas sales.salesorder header y sales.salesperson 
--     para sacar los campos : SalesOrderID, SalesQuota, Bonus
SELECT SalesOrderID
		,SalesQuota
		,Bonus
FROM  Sales.SalesOrderHeader s
JOIN Sales.SalesPerson p ON s.SalesPersonID = p.BusinessEntityID


-- 51. Despliega del catalogo de productos la descripci�n de cada producto utilizando 
--     las tablas production.product y production.productmodel
SELECT p.ProductID
		,p.Name
		,p.ProductNumber
		,p.MakeFlag
		,m.Name AS  'productModel'
		,m.CatalogDescription
FROM production.product p
JOIN production.productmodel m ON p.ProductModelID = m.ProductModelID

------------- Parte 13 --------------
-------------------------------------
-- 52. Realiza un query donde saquemos el nombre productid y salesorderid de las tablas production.product y sales.salesorderdetail. 
SELECT p.ProductID
		,p.Name AS 'nameProduct'
		,s.salesorderid
FROM production.product p
JOIN sales.salesorderdetail s ON p.ProductID = s.ProductID

-- 53. Cambia el query para el ejercicio 1 para ver solamente los productos que no han sido ordenados. Es decir, que no tengan un salesorderid. 
SELECT p.ProductID
		,p.Name AS 'nameProduct'
		,s.salesorderid
FROM production.product p
LEFT JOIN sales.salesorderdetail s ON p.ProductID = s.ProductID
WHERE s.salesorderid IS NULL

-- 54. Escribir una consulta donde relacionemos las tablas sales.salesperson y sales.salesorderheader 
--    donde las relacionamos por los campos businessentityid y salespersonid. 
SELECT *
FROM sales.salesperson p 
JOIN sales.salesorderheader s ON p.BusinessEntityID = s.salespersonid


-- 55. Cambia la consulta del ejercicio anterior para mostrar tambi�n el nombre de la persona y su apellido.
SELECT  p.businessentityid
		,s.salespersonid
		,per.FirstName
		,per.LastName
FROM sales.salesperson p 
JOIN sales.salesorderheader s ON p.BusinessEntityID = s.salespersonid
JOIN HumanResources.Employee e ON  p.BusinessEntityID = e.BusinessEntityID
JOIN Person.Person per ON per.BusinessEntityID = e.BusinessEntityID

SELECT *
FROM sales.salesperson p 
JOIN sales.salesorderheader s ON p.BusinessEntityID = s.salespersonid
JOIN Sales.vSalesPerson dp ON  p.BusinessEntityID = dp.BusinessEntityID


-- 56. La tabla Sales.SalesOrderHeader contiene claves externas para las tablas Sales.CurrencyRate y Purchasing.ShipMethod. 
--     Escriba una consulta que una las tres tablas, asegur�ndose de que contenga todas las filas de Sales.SalesOrderHeader.
--     Incluya las columnas CurrencyRateID, AverageRate, SalesOrderID y ShipBase.
SELECT s.CurrencyRateID
		,AverageRate
		,SalesOrderID
		,ShipBase
FROM Sales.SalesOrderHeader s
JOIN Sales.CurrencyRate c ON s.CurrencyRateID = c.CurrencyRateID
JOIN Purchasing.ShipMethod ps ON s.ShipMethodID = ps.ShipMethodID

-- 57. Escriba una consulta que devuelva la columna BusinessEntityID de la tabla Sales.SalesPerson
--     junto con cada ProductID de la tabla Production.Product.
SELECT p.BusinessEntityID
		,pr.ProductID
FROM Sales.SalesPerson p
JOIN Sales.SalesOrderHeader sh ON p.BusinessEntityID = sh.SalesPersonID
JOIN Sales.SalesOrderDetail sd ON sh.SalesOrderID = sd.SalesOrderID
JOIN Production.Product pr ON sd.ProductID = pr.ProductID


------------- Parte 14 --------------
-------------------------------------

-- 58. Mediante una subconsulta, muestre los nombres de productos y los n�meros de ID de productos de la tabla Production.Product que se han pedido.
SELECT DISTINCT prod.ProductID,
		prod.Name
FROM production.product prod 
WHERE prod.ProductID IN (SELECT DISTINCT p.ProductID
							FROM production.product p
							LEFT JOIN sales.salesorderdetail s ON p.ProductID = s.ProductID
							WHERE s.salesorderid IS NOT NULL)
ORDER BY prod.ProductID



-- 59. Cambie la consulta escrita en la pregunta 1 para mostrar los productos que no se han pedido.
SELECT DISTINCT prod.ProductID,
		prod.Name
FROM production.product prod 
WHERE prod.ProductID IN (SELECT DISTINCT p.ProductID
							FROM production.product p
							LEFT JOIN sales.salesorderdetail s ON p.ProductID = s.ProductID
							WHERE s.salesorderid IS NULL)
ORDER BY prod.ProductID

/************************************************************************************************************
-------- Para la 60 y 61, no esta la tabla productColor --------
60. Escriba una consulta mediante una subconsulta que devuelva las filas de
    la tabla Production.ProductColor que no se utilizan en la tabla Production.Product.
61. Escriba una consulta que muestre los colores utilizados en la tabla Production.Product
   que no aparecen en la tabla Production.ProductColor mediante una subconsulta.
   Utilice la palabra clave DISTINCT antes del nombre de la columna para devolver cada color solo una vez.
************************************************************************************************************/


-- 62. Escribe una consulta que une ModifiedDate de Person.Person y HireDate de HumanResources.Employee.
SELECT p.BusinessEntityID
		,p.ModifiedDate
		,e.HireDate
FROM Person.Person p
JOIN HumanResources.Employee e ON p.BusinessEntityID = e.BusinessEntityID

------------- Parte 15 --------------
-------------------------------------
-- 63. Una la tabla Sales.SalesOrderHeader a la tabla Sales.SalesOrderDetail. Muestre las columnas 
--     SalesOrderID, OrderDate y ProductID en los resultados. La tabla Sales.SalesOrderDetail debe estar en una subconsulta para hacer el join
SELECT sh.SalesOrderID
		,sh.OrderDate
		,sd.ProductID
FROM Sales.SalesOrderHeader sh
JOIN Sales.SalesOrderDetail sd ON sh.SalesOrderID = sd.SalesOrderID

-- 64. Reescribe el query del eercicio 1 con una tabla de expresiones  comunes. 
WITH productos 
AS
(SELECT sh.SalesOrderID
		,sh.OrderDate
		,sd.ProductID
FROM Sales.SalesOrderHeader sh
JOIN Sales.SalesOrderDetail sd ON sh.SalesOrderID = sd.SalesOrderID
)
SELECT *
FROM productos


------------- Parte 16 --------------
-------------------------------------
-- 65. Escriba una consulta para determinar el n�mero de clientes en la tabla Sales.Customer.
SELECT COUNT(CustomerID) AS 'n�meroClientes'
FROM Sales.Customer c

-- 66.Escriba una consulta que enumere el n�mero total de productos pedidos.
--    Utilice la columna OrderQty de la tabla Sales.SalesOrderDetail y la funci�n SUM
SELECT sd.SalesOrderID
		,SUM(sd.OrderQty) AS 'OrderQty'
FROM Sales.SalesOrderDetail sd
GROUP BY sd.SalesOrderID

-- 67. Escriba una consulta para determinar el precio del producto m�s caro pedido. 
--    Utilice la columna UnitPrice de la tabla Sales.SalesOrderDetail.
SELECT MAX(sd.UnitPrice ) 'precioAlto'
FROM Sales.SalesOrderDetail sd

--68. Escriba una consulta para determinar el importe medio del flete en la tabla Sales.SalesOrderHeader.
SELECT AVG(TaxAmt) AS 'promedioFlete'
FROM Sales.SalesOrderHeader s

--69. Escriba una consulta utilizando la tabla Production.Product que muestre el ListPrice m�nimo, m�ximo y promedio.
SELECT MIN(p.ListPrice) AS 'm�nimo'
		,MAX(p.ListPrice) AS 'm�ximo'
		,AVG(p.ListPrice) AS 'promedio'
FROM Production.Product p


------------- Parte 17 --------------
-------------------------------------
-- 70. Escriba una consulta que muestre el n�mero total de art�culos pedidos para cada producto.
--     Utilice la tabla Sales.SalesOrderDetail para escribir la consulta.
SELECT sd.ProductID AS 'producto',
		SUM(sd.OrderQty) AS 'numTotal' 
FROM Sales.SalesOrderDetail sd
GROUP BY sd.ProductID

-- 71. Escriba una consulta utilizando la tabla Sales.SalesOrderDetail que muestra un recuento de las l�neas de detalle para cada SalesOrderID.
SELECT sd.SalesOrderID
		,COUNT(sd.SalesOrderDetailID) AS 'recuentoLineas'
FROM Sales.SalesOrderDetail sd
GROUP BY sd.SalesOrderID
ORDER BY 1

-- 72. Escriba una consulta usando la tabla Production.Product que enumere un recuento de los productos en cada l�nea de productos.
SELECT p.ProductLine AS 'linea'
		,COUNT(p.ProductID) AS 'recuento'
FROM Production.Product p
GROUP BY p.ProductLine 
ORDER BY 1

-- 73. Escriba una consulta que muestre el recuento de pedidos realizados por a�o para cada cliente mediante la tabla Sales.SalesOrderHeader.
SELECT s.CustomerID
		,DATEPART(YEAR, s.OrderDate) AS 'a�o'
		,COUNT(s.SalesOrderID) AS 'recuentoPedidos'
FROM Sales.SalesOrderHeader s
GROUP BY s.CustomerID,DATEPART(YEAR, s.OrderDate)
ORDER BY 1, 2

------------- Parte 18 --------------
-------------------------------------
-- 74. Escriba una consulta que devuelva un recuento de l�neas de detalle en la tabla Sales.SalesOrderDetail por SalesOrderID.
--     Incluya solo aquellas ventas que tengan m�s de tres l�neas de detalle.
SELECT sd.SalesOrderID
		,COUNT(sd.SalesOrderDetailID) AS 'recuentoLineas'
FROM Sales.SalesOrderDetail sd
GROUP BY sd.SalesOrderID
HAVING  COUNT(sd.SalesOrderDetailID) > 3
ORDER BY 1

-- 75. Escriba una consulta que cree una suma de LineTotal en la tabla Sales.SalesOrderDetail agrupada por SalesOrderID.
--     Incluya solo aquellas filas donde la suma exceda 1,000.
SELECT SalesOrderID
		,SUM(LineTotal) AS 'sumLineTotal'
FROM Sales.SalesOrderDetail
GROUP BY SalesOrderID
HAVING SUM(LineTotal) > 1000
ORDER BY SalesOrderID

-- 76. Escriba una consulta que agrupe los productos por ProductModelID junto con un conteo. Muestra las filas que tienen un recuento igual a 1.
SELECT p.ProductModelID
		,COUNT(p.ProductID)
FROM Production.Product p
GROUP BY p.ProductModelID
HAVING COUNT(p.ProductID) = 1


-- 77. Modificar la consulta de la pregunta 3 para que solo se incluyan los productos con el color azul o rojo.
SELECT p.ProductModelID
		,COUNT(p.ProductID)
FROM Production.Product p
WHERE p.Color IN ('Blue', 'Red')
GROUP BY p.ProductModelID

------------- Parte 19 --------------
-------------------------------------
-- 78. Escriba una consulta utilizando la tabla Sales.SalesOrderDetail para obtener un recuento de los valores �nicos de ProductID que se han pedido.
SELECT SalesOrderID
		,COUNT(DISTINCT ProductID) AS 'unicosProductos'
FROM Sales.SalesOrderDetail
GROUP BY SalesOrderID


-- 79. Escriba una consulta utilizando la tabla Sales.SalesOrderHeader que devuelva el recuento de valores TerritoryID �nicos por cliente.
SELECT CustomerID,
		COUNT(DISTINCT TerritoryID)
FROM Sales.SalesOrderHeader
GROUP BY CustomerID
ORDER BY CustomerID

------------- Parte 20 --------------
-------------------------------------
-- 80. Escriba una consulta que una las tablas Person.Person, Sales.Customer y Sales.SalesOrderHeader para devolver una lista de los nombres de los clientes junto con un recuento de los pedidos realizados.
SELECT c.CustomerID
		,CONCAT(p.FirstName, ' ', p.LastName) AS 'cliente'
		,COUNT(s.SalesOrderID) AS 'recuentoPedido'
FROM Person.Person p
JOIN Sales.Customer c ON p.BusinessEntityID = c.PersonID
JOIN Sales.SalesOrderHeader s ON s.CustomerID = c.CustomerID 
GROUP BY c.CustomerID, CONCAT(p.FirstName, ' ', p.LastName)


-- 81. Escriba una consulta utilizando Sales.SalesOrderHeader, Sales.SalesOrderDetail y Tablas Production.Product 
--     para mostrar la suma total de productos por ProductID y OrderDate
SELECT s.OrderDate
		,p.ProductID
		,p.Name
		,SUM(d.OrderQty) AS 'sumaTotalProductos'
FROM Sales.SalesOrderHeader s
JOIN Sales.SalesOrderDetail d ON  s.SalesOrderID = d.SalesOrderID
JOIN Production.Product p ON p.ProductID = d.ProductID
GROUP BY p.ProductID, s.OrderDate, p.Name

------------- Parte 21 --------------
-------------------------------------
-- 82. Escriba una consulta que una la tabla HumanResources.Employee con la tabla Person.Person para que puede
--     mostrar las columnas FirstName, LastName y HireDate para cada empleado. 
--     Mostrar el JobTitle junto con un recuento de empleados para el t�tulo. Utilice una tabla derivada para resolver esta consulta.
SELECT JobTitle
		,COUNT(idEmpleado) AS 'recuentoEmpleado'
FROM (SELECT e.BusinessEntityID AS idEmpleado
				,p.FirstName AS FirstName
				,p.LastName AS LastName
				,e.JobTitle AS JobTitle
				,e.HireDate AS HireDate
		FROM HumanResources.Employee e
		JOIN  Person.Person p ON  e.BusinessEntityID = p.BusinessEntityID ) AS tabla_derivada
GROUP BY JobTitle

-- 83. Vuelva a escribir la consulta de la pregunta 1 usando un CTE.
WITH cte_empleados AS (
	SELECT e.BusinessEntityID AS idEmpleado
			,p.FirstName AS FirstName
			,p.LastName AS LastName
			,e.JobTitle AS JobTitle
			,e.HireDate AS HireDate
	FROM HumanResources.Employee e
	JOIN  Person.Person p ON  e.BusinessEntityID = p.BusinessEntityID 
)
SELECT JobTitle
		,COUNT(idEmpleado) AS 'recuentoEmpleado'
FROM cte_empleados 
GROUP BY JobTitle


-- 84. Muestre CustomerID, SalesOrderID y OrderDate para cada fila Sales.SalesOrderHeader como siempre que el cliente haya realizado al menos cinco pedidos. Se pude usar CTE, join o subquery.
WITH cte_pedidos AS (
	SELECT CustomerID
			,COUNT(SalesOrderID) AS 'cantPedidos'
	FROM Sales.SalesOrderHeader
	GROUP BY CustomerID
)

SELECT s.CustomerID
		,s.SalesOrderID
		,s.OrderDate
FROM Sales.SalesOrderHeader s
WHERE s.CustomerID IN (SELECT CustomerID 
						FROM cte_pedidos cte
						WHERE cte.cantPedidos >= 5)




---------- Parte 22 - 23 ------------
-------------------------------------
/* 
-- SE REPITE, 80 - 85 - 88
-- 85. Escriba una consulta que una las tablas Person.Person, Sales.Customer y Sales.SalesOrderHeader para obtener una lista de los nombres de los clientes junto con un recuento de los pedidos realizados.
*/

/* 
--- SE REPITE, 81 - 86 - 87 - 89 -90
-- 86. Escriba una consulta utilizando Sales.SalesOrderHeader, Sales.SalesOrderDetail y
-- 87. Tablas Production.Product para mostrar la suma total de productos por ProductID y OrderDate.
*/

------------- Parte 24 --------------
-------------------------------------
-- 91. Escriba un script que declare una variable entera llamada @myInt. Asigne 10 a la variable y luego Impr�melo.
DECLARE @myInt INT;
SET @myInt = 10;
PRINT @myInt;

-- 92. Escriba un script que declare una variable VARCHAR(20) llamada @myString. Asignar Esta es una prueba para el variable e imprimalo.
DECLARE @myString VARCHAR(20);
SET @myString = 'prueba';
PRINT @myString;

-- 93. Escriba un script que declare dos variables enteras llamadas @MaxID y @MinID. 
--     Usa  las variables para imprimir los valores m�s altos y m�s bajos de SalesOrderID de la tabla Sales.SalesOrderHeader.
DECLARE @MaxID INT,  @MinID INT;
SET @MaxID = (SELECT MAX(SalesOrderID) FROM Sales.SalesOrderHeader)
PRINT @MaxID;
SET @MinID = (SELECT MIN(SalesOrderID) FROM Sales.SalesOrderHeader)
PRINT @MinID;

-- 94. Escriba un script que declare una variable entera llamada @ID. Asigne el valor 70000 a la variable. Utilice la variable en una sentencia 
--     SELECT que devuelva todos los valores de SalesOrderID del Tabla Sales.SalesOrderHeader que tiene un SalesOrderID mayor que el valor de la variable.
DECLARE @ID INT;
SET @ID = 7000;
SELECT SalesOrderID
FROM Sales.SalesOrderHeader
WHERE SalesOrderID > @ID;

-- 95. Escriba un script que declare tres variables, una variable entera llamada @ID, una NVARCHAR(50) variable denominada @FirstName y una variable NVARCHAR(50) denominada @LastName. 
--     Utilice una SELECCI�N declaraci�n para establecer el valor de las variables con la fila de la tabla Person.Person con BusinessEntityID = 1.
--     Imprima una declaraci�n en el formato "BusinessEntityID: FirstName LastName".
DECLARE @ID INT,  @FirstName VARCHAR(50), @LastName VARCHAR(50);
SET @ID = 1;
SET @FirstName = (SELECT FirstName  FROM Person.Person WHERE BusinessEntityID = @ID);
SET @LastName = (SELECT LastName  FROM Person.Person WHERE BusinessEntityID = @ID);
PRINT (CAST(@ID AS VARCHAR)+':'+@FirstName + ' ' + @LastName );


-- 96. Escriba un script que declare una variable entera llamada @SalesCount. 
--     Establezca el valor de la variable en el recuento total de ventas en la tabla Sales.SalesOrderHeader. 
--    Usa la variable en un SELECT declaraci�n que muestra la diferencia entre @SalesCount y el recuento de ventas por cliente.
DECLARE @SalesCount INT;
SET @SalesCount = (SELECT COUNT(SalesOrderID) FROM Sales.SalesOrderHeader);
SELECT CustomerID
		,(@SalesCount - COUNT(SalesOrderID)) AS 'diferencia'
FROM Sales.SalesOrderHeader
GROUP BY CustomerID

------------- Parte 25 --------------
-------------------------------------
-- 97.Cree una tabla temporal llamada #CustomerInfo que contenga CustomerID, FirstName y LastName columnas 
--    Incluya las columnas CountOfSales y SumOfTotalDue.
--    Rellenar la tabla con una consulta utilizando las tablas Sales.Customer, Person.Person y Sales.SalesOrderHeader.
SELECT c.CustomerID
		,p.FirstName
		,p.LastName
		,COUNT(c.CustomerID) AS CountOfSales
		,SUM(so.TotalDue) AS SumOfTotalDue
INTO #CustomerInfo
FROM Sales.Customer c
JOIN Person.Person p ON c.PersonID = p.BusinessEntityID 
JOIN Sales.SalesOrderHeader so ON c.CustomerID = so.CustomerID
GROUP BY c.CustomerID ,p.FirstName ,p.LastName 

SELECT *
FROM #CustomerInfo

-- 98. Cambie el c�digo escrito en la pregunta 1 para usar una variable de tabla en lugar de una tabla temporal.
DECLARE @sql_consulta VARCHAR (330); 
SET @sql_consulta = 'SELECT c.CustomerID
		,p.FirstName
		,p.LastName
		,COUNT(c.CustomerID) AS CountOfSales
		,SUM(so.TotalDue) AS SumOfTotalDue
FROM Sales.Customer c
JOIN Person.Person p ON c.PersonID = p.BusinessEntityID 
JOIN Sales.SalesOrderHeader so ON c.CustomerID = so.CustomerID
GROUP BY c.CustomerID ,p.FirstName ,p.LastName'
PRINT @sql_consulta


-------------- Parte 26 --------------
--------------------------------------

-- 99. Cree una tabla llamada dbo.testCustomer. 
-- Incluya un CustomerID que sea una columna de identidad principal llave.
-- Incluya las columnas FirstName y LastName. 
-- Incluya una columna Edad con una restricci�n de verificaci�n especificando que el valor debe ser inferior a 120. 
-- Incluya una columna �Activa� que sea de un solo car�cter con un valor predeterminado de Y y solo permite Y o N. Agregue algunas filas a la tabla.

DROP TABLE AdventureWorks2019.dbo.testCustomer
-- Forma 1 
CREATE TABLE AdventureWorks2019.dbo.testCustomer(
	CustomerID INT NOT NULL,
	FirstName VARCHAR(255),
	LastName VARCHAR(255),
	Age int CHECK (Age < 120),
	Activa VARCHAR(1) DEFAULT 'Y' NOT NULL CHECK(Activa IN ('Y', 'N')),
	h VARCHAR(5) DEFAULT 'hola',
	CONSTRAINT PK_TestCustomer PRIMARY KEY (CustomerID)
)

DROP TABLE AdventureWorks2019.dbo.testCustomer
-- Forma 2 
CREATE TABLE AdventureWorks2019.dbo.testCustomer(
	CustomerID INT NOT NULL,
	FirstName VARCHAR(255),
	LastName VARCHAR(255),
	Age int CHECK (Age < 120),
	Activa VARCHAR(1) DEFAULT 'Y',
	CONSTRAINT PK_TestCustomer PRIMARY KEY (CustomerID),
	CONSTRAINT chk check(Activa IN ('Y', 'N'))
)

INSERT INTO testCustomer VALUES (2,'Nazareth', 'Suarez', 26, 'N');
INSERT INTO testCustomer VALUES (3,'Victor', 'Mendoza', 28, 'X');
INSERT INTO testCustomer VALUES (4,'Andres', 'Mendoza', 150, 'Y');
INSERT INTO testCustomer VALUES (5,'Juan Manuel', 'Mendoza',23);
INSERT INTO testCustomer(CustomerID, FirstName, LastName, Age ) VALUES (6,'Mayer', 'Aguilera', 30);
INSERT INTO testCustomer(CustomerID, FirstName, LastName, Age ) VALUES (7,'Daniel', 'ospino', 28);
INSERT INTO testCustomer(CustomerID, FirstName, LastName, Age ) VALUES (8,'Alejandra', 'Guzman', 25);

SELECT * FROM testCustomer

-- 100. Cree una tabla llamada dbo.testOrder.
-- Incluya una columna CustomerID que sea una clave externa que apunte a dbo.testCustomer. 
-- Incluya una columna OrderID que sea una clave principal de columna de identidad.
-- Incluir un Columna OrderDate que por defecto es la fecha y hora actual.
-- Incluya una columna ROWVERSION. Agregar algunas filas a la tabla.

DROP TABLE AdventureWorks2019.dbo.testOrder;

CREATE TABLE AdventureWorks2019.dbo.testOrder(
	OrderID INT  NOT NULL,
	CustomerID INT NOT NULL,
	OrderDate DATETIME DEFAULT GETDATE(),
	RV ROWVERSION,
	PRIMARY KEY (OrderID),
    FOREIGN KEY (CustomerID) REFERENCES testCustomer(CustomerID)
)

INSERT INTO testOrder(OrderID, CustomerID) VALUES (1, 2);   
INSERT INTO testOrder(OrderID, CustomerID) VALUES (2, 2);
INSERT INTO testOrder(OrderID, CustomerID) VALUES (3, 6);
INSERT INTO testOrder(OrderID, CustomerID) VALUES (4, 6);

SELECT *
FROM testOrder

--------------- Parte 27 --------------
---------------------------------------
-- 101. Cree una vista denominada dbo.vw_Products que muestre una lista de los productos de la Tabla Production.Product unida a la tabla Production.ProductCostHistory. 
--      Incluir columnas que describen el producto y muestran el historial de costos de cada producto. Pruebe la vista creando un consulta que recupera datos de la vista.

DROP VIEW dbo.vw_Products;

CREATE VIEW dbo.vw_Products
AS
SELECT p.ProductID AS productoID
		,p.Name AS productoNombre
		,p.ProductNumber AS codigoProducto
		,p.MakeFlag AS esComprado 
		,p.StandardCost AS productoPrecioStand
		,p.DaysToManufacture AS diasManufacturacion
		,ph.StartDate AS fechaInicioStand
		,ph.EndDate AS fechaFinStand
		,ph.StandardCost AS historialPrecioStand
FROM Production.Product p
JOIN Production.ProductCostHistory ph ON p.ProductID = ph.ProductID

SELECT *
FROM vw_Products

SELECT p.*
FROM vw_Products p
WHERE p.esComprado = 1 


-- 102. Cree una vista llamada dbo.vw_CustomerTotals que muestre:
--      las ventas totales del TotalDue columna por a�o y mes para cada cliente. 
--      Pruebe la vista creando una consulta que recupere datos de la vista.

DROP VIEW dbo.vw_CustomerTotals;

CREATE VIEW dbo.vw_CustomerTotals
AS
SELECT c.CustomerID
		,DATEPART(YEAR, s.OrderDate) AS 'a�o'
		,DATEPART(MONTH, s.OrderDate) AS 'mes'
		,SUM(s.TotalDue) AS 'totalDue' 
FROM sales.Customer c
JOIN Sales.SalesOrderHeader s ON c.CustomerID = s.CustomerID 
GROUP BY c.CustomerID,DATEPART(YEAR, s.OrderDate), DATEPART(MONTH, s.OrderDate) 


SELECT vc.CustomerID,
		vc.a�o,
		vc.totalDue
FROM dbo.vw_CustomerTotals vc
GROUP BY vc.CustomerID, vc.a�o,vc.totalDue



